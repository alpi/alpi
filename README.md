# The ALPI tasking interface

The ALPI project defines a lowlevel interface [alpi.h](alpi.h) that any tasking
runtime system can implement to support the external task-aware libraries that
enable interoperability between tasks and well-know programming models, such as
MPI, CUDA, etc. The task-aware libraries leverage a minimal set of tasking
runtime system procedures to spawn and manage asynchronous tasks.

Any tasking runtime system can support these libraries by implementing and
exposing the [alpi.h](alpi.h) interface.

The name of ALPI stands for Asynchronous Low-level Programming Interface and is
pronounced as the catalan word *alpí* (which means *alpine*).

Some of the task-aware libraries that leverage this interface are:

* [Task-Aware MPI](https://github.com/bsc-pm/tampi)
* [Task-Aware CUDA](https://github.com/bsc-pm/tacuda)
* [Task-Aware HIP](https://github.com/bsc-pm/tahip)
* [Task-Aware SYCL](https://github.com/bsc-pm/tasycl)
* [Task-Aware GASPI](https://github.com/bsc-pm/tagaspi)
* [Task-Aware LPF](https://github.com/bsc-pm/talpf)
* [Task-Aware AscendCL](https://github.com/bsc-pm/tacl)

The tasking runtime systems that implement this interface are:

* The [nOS-V](nosv) lowlevel tasking runtime system.
* The [NODES](nodes) tasking runtime system that supports the [OmpSs-2](ompss-2)
  model. This runtime system leverages nOS-V and the ALPI interface is available
  because nOS-V implements it.
* The [Nanos6](nanos6) tasking runtime system that supports the [OmpSs-2](ompss-2)
  tasking model.
* The derivative [LLVM/OpenMP](llvm-openmp) tasking runtime system that supports
  the [OpenMP][openmp] model. This derivative runtime system leverages nOS-V and
  the ALPI interface is available because nOS-V implements it.

[nosv]: https:://github.com/bsc-pm/nos-v
[nodes]: https:://github.com/bsc-pm/nodes
[nanos6]: https:://github.com/bsc-pm/nanos6
[ompss-2]: https://github.com/bsc-pm/ompss-2-releases
[openmp]: https://www.openmp.org
[llvm-openmp]: https:://github.com/bsc-pm/llvm

## Licensing

The code of the ALPI interface is lincensed under the clauses of the MIT license
included in the [COPYING](COPYING) file. The copyright of the files of this
project belongs to the Barcelona Supercomputing Center (BSC), unless otherwise
explicitly stated.

## Documentation

The interface header [alpi.h](alpi.h) is documented following the Doxygen format
and this repository contains a Doxyfile. You can generate the documentation in
html or PDF format using the `doxygen` tools. Visit the [Doxygen website](doxygen)
to obtain information about how to install it.

You can generate the documentation by running the command:

```sh
$ doxygen Doxyfile
```

This command generates the `build-docs` output directory with the `html` and
`latex` sub-directories. You can see the HTML documentation by opening the
`build-docs/html/index.html` file with any HTML viewer program.

If you want to see the PDF documentation, you should run the following extra
commands to generate the PDF file:

```sh
$ cd build-docs/latex
$ make
```

This command generates a PDF file in the `build-docs/latex` sub-directory
that you may open with any PDF viewer program.

[doxygen]: https://www.doxygen.nl/index.html
